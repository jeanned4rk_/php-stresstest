# php-stresstest

This image will stress the CPU for `STRESS_DURATION` seconds (env variable).


Used to check your horizontal pod scaling

https://hub.docker.com/r/jeanned4rk/php-stresstest