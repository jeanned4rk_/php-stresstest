<pre>

<?php
$starttime = microtime(true);
$delay = getenv('STRESS_DURATION');
$end = $starttime + $delay;
$i = 0;
// echo "Delay : " . $delay . "<br>";
// echo "Start : " . $starttime . "<br>";
while(microtime(true) < $end){
    $i++;
}
// echo "Expected end : " . $end . "<br>";
// echo "Actual end : " . microtime(true) . "<br>";
// echo gethostname();

$data = [
    "Delay" => $delay,
    "Start" => $starttime,
    "Expected end" => $end,
    "Actuel end" => microtime(true),
    "Hostname" => gethostname()
];

print_r($data);

?>

</pre>